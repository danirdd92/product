// only release/* is tracked

def nextVersion

pipeline {
    agent any
    tools {
        maven 'maven'
        jdk 'jdk-8'
    }
    stages {
        stage('Version') {
            environment {
                BRANCH_VERSION = "${env.BRANCH_NAME.tokenize('/')[1]}"
            }
            steps {
                // managed by declerative checkout - sh 'git fetch --tags'

                script {
                    def latestVersion = sh(returnStdout: true, script: "git tag -l --sort=version:refname \"${BRANCH_VERSION}.*\" | tail -1 || echo 'none'").trim()
                    patchNumber = 0

                    if (latestVersion != 'none') {
                        def parts = latestVersion.tokenize('.')
                        if (parts.size() > 2) {
                            patchNumber = parts[2].toInteger()
                        }
                        patchNumber++
                    }

                    nextVersion = "${BRANCH_VERSION}.${patchNumber}"
                    sh "mvn versions:set -DnewVersion=${nextVersion}"
                }
            }
        }
        stage('Build') {
            steps {
                sh 'mvn clean compile'
            }
        }
        stage('Unit Test') {
            steps {
                sh 'mvn test'
            }
        }
        stage('Package') {
            steps {
                sh 'mvn package -DskipTests'
            }
        }
        stage('E2E Test') {
            steps {
                echo 'E2E Test TODO'
            }
        }
        stage('Publish') {
            steps {
                sh 'mvn deploy -DskipTests'
                sh "git clean -f && git tag -a ${nextVersion} -m 'Release ${nextVersion}'"
                sh 'git push origin --tags'
            }
        }
    }
}
